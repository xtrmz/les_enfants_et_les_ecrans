# Qui nous sommes ?

* pères de famille
* représentants de parents d'élèves
* travaillant dans le secteur informatique
* membres de l'association Les enfants d'abord

![](enfantsdabord.png)

# Gestion des écrans et contrôle parental
## Problématique

::: incremental

* "Je ne sais pas comment faire en sorte que mon enfant arrête de jouer aux vidéos."
* "Alex, tu laisses ton téléphone et tu viens à table ! C'est la 3ème fois que je te le demande"
* "Mais qu'est-ce qu'il fait encore sur la tablette ?"
:::

# Conseils de professionnels

* En 2019 - rencontre avec médecin du scolaire et psychologue
* découverte des 3/6/9/12 de Serge Tisseron
![](img/36912.png)

:::notes
:::

# 3/6/9/12 de Serge Tisseron

* Psychiatre et docteur en psychologie

![](img/36912_2.png)

:::notes
beaucoup de conseils sur l'accompagnement des enfants devant les écrans
Avant 3 ans : Jouons, parlons, arrêtons la télé.
De 3 à 6 ans : limitons les écrans, partageons-les, parlons en famille
De 6 à 9 ans : créons avec les écrans, expliquons-lui Internet
De 9 à 12 ans : apprenons-lui à se protéger et à protéger ses échanges
Après 12 ans : Restons disponibles
:::
#  Objectifs

* apporter des solutions concrètes 🤔
* trouver des alternatives aux écrans :tv:

:::notes
cherche à avoir un discours ne pas diaboliser les parents
nous ne sommes pas des parents modèles
:::

# Quelques risques liés aux écrans

* Sommeil sur les enfants :bed:
* Contenus choquants, inapropriés 😱
* Confidentialités (fuite de données)
* Micropaiement et jeux viraux :moneybag:

# Controle parental
* A quoi cela sert ?
* Pourquoi le mettre en place ?

:::notes

permet aux parents de restreindre automatiquement l’accès de leurs enfants à un média (Internet, télévision, console de jeu) en le limitant à certaines catégories de contenus, afin de les protéger. Un premier risque est lié à la pédophilie et aux contenus inappropriés considérés comme choquants pour leur âge (pornographie, violence, fausse nouvelles et contenus mensongers).
source: wikipedia

:::
# Controle parental sur android

* Bien-être numérique (live)
* Gestion multi-poste : application family link
![](img/google-family-link-logo-1.jpg)

:::notes
Bien numérique - constat mesure de l'activité sur internet par application
family link : permet de surveiller/controler...
:::

# Controle parental sur iOS (Apple)

* Application "Temps d'écran"
* Gestion multi-poste : Compte iCloud pour enfant

:::notes
Temps d'écran application sur iOS quasi identique à Bien être numérique
:::

# Controle parental sur consoles de jeu

* semble assez efficace sur la switch
* plus difficile sur playstation

:::notes

efficace sur Switch beaucoup de fonctionnalités
difficile sur playstation (apparemment plus difficile à configurer)
:::

# {data-background="img/Socialmedia-pm.png" width=200px;}

# Réseaux sociaux

* :warning: interdit au moins de 13 ans
* Importance des photos
* :warning: Le harcèlement
* la dépendance

:::notes
lisez vous les conditions d'utilisations des sites sur lesquels vous vous abonnez , des services que vous utilisez...
:::

# Conditions d'utilisations d'instagram {data-background="img/insta.svg" width=200px;}
(expliquées à un enfant de 8 ans)

:::notes
En 2017, une avocate Jenny Afia mandatée par les services du délégué britannique aux droits des enfants a retranscris l'ensemble des conditions d'utilisation d'Instagram pour qu'un enfant de 8 ans puisse les comprendre.
:::

# {data-background="img/insta.svg" width=200px;}

<blockquote>2. Officiellement, tu es propriétaire des photos et vidéos que tu postes, mais nous avons le droit de les utiliser, et de laisser d'autres personnes les utiliser, partout dans le monde. Les gens nous paient pour les utiliser, et nous ne te paierons pas.
</blockquote>

# {data-background="img/insta.svg" width=200px;}

<blockquote>
5. Même si tu es responsable des informations que tu mets sur Instagram, nous pouvons les garder, les utiliser et les partager avec des entreprises connectées à Instagram. Cela inclut ton nom, ton adresse mail, ton école, où tu vis, tes photos, ton numéro de téléphone, tes "likes" et "dislikes", où tu vas, où tes amis vont, combien de fois tu utilises Instagram, ta date d'anniversaire, à qui tu parles ainsi que tes messages privés.
</blockquote>

# Jeux vidéos

* addictif, jeux viraux
* :warning: intègre souvent un réseau social

# ![](img/pegi.png)

![](img/pegi_desc.png)

:::notes
Le système PEGI de classification par âge des jeux vidéo est utilisé dans 38 pays européens. La classification par âge confirme que le jeu est approprié à l’âge du joueur. La classification PEGI se base sur le caractère adapté d’un jeu à une classe d’âge, et non sur le niveau de difficulté.
:::

# Quelques exemples

* Brawl Star **PEGI 7**
* Fortnite **PEGI 12** :warning:
* Clash of Clan **PEGI 12**
* Assassin Creed Valhalla **PEGI 18**

# Alternatives aux jeux violents

* Subnautica
* Minecraft
* Monument Valley
![](img/Monument-Valley.jpg)

# Live

* [freebox](http://mafreebox.freebox.fr)
* [Console switch](https://www.e-enfance.org/actualite/nintendo-switch/)

# Conseils (quand c'est possible)

* Montrer l'exemple
* Accompagner devant les écrans
* limiter le temps (couper le wifi le soir)
* Faire attention avec le sommeil
* Pas d'écran dans la chambre, pas d'écran en continu, pause.



# La suite

* Semaine sans écran sur Bègles ?

:::notes
ici j'ai envie de parler .....
* Engagement moral des parents pour ne pas avoir de téléphone portable pour les enfants de 6ème.
:::

# Solutions alternatives

* écouter la musique
* écouter des podcasts
* faire du sport
* jouer aux jeux de sociétés
* sortir en extérieur
 
# Quelques ressources

* Conditions d'utilisations d'[instagram](https://www.businessinsider.fr/une-avocate-a-reecrit-les-conditions-dutilisations-dinstagram-comme-si-elles-etaient-expliquees-a-un-enfant-de-8-ans/)
* [Gestion des écrans](https://www.3-6-9-12.org)
* [espace controle parental](https://www.e-enfance.org/espace-controle-parental)
* [Les promeneurs du net](https://www.promeneursdunet.fr/)
* [je protege mon enfant](https://jeprotegemonenfant.gouv.fr/vos-outils/)
* [PEGI](https://pegi.info/fr)
* [Le créateur du scroll infini cherche aujourd'hui des parades à son invention](https://www.francetvinfo.fr/internet/telephonie/video-le-createur-du-scroll-infini-sur-smartphone-cherche-aujourd-hui-des-parades-a-son-invention_4101423.html)

# merci

votre avis
![](img/qrcode_sondage.png)
